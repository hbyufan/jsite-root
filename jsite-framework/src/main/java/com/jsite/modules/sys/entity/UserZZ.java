/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jsite.modules.sys.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;
import com.jsite.common.config.Global;
import com.jsite.common.persistence.DataEntity;
import com.jsite.common.utils.Collections3;
import com.jsite.common.utils.excel.annotation.ExcelField;
import com.jsite.common.utils.excel.fieldtype.RoleListType;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * 用户Entity
 * @author ThinkGem
 * @version 2013-12-05
 */
public class UserZZ  {

	private static final long serialVersionUID = 1L;
	private Long a0100;//人员编号
	private String a0101;//姓名
	private String a0102;//拼因
	private String a01k29;
	private String a0104;
	private String a0134;
	private String a01k66;
	private String a0144;
	private String a0141b;
	private String a0141a;
	private String a0837;
	private String a0801n;
	private String a0145;
	private String a0104n;
	private String a0117n;
	private String a0107;
	private String a0111n;
	private String a01k33;
	
	public Long getA0100() {
		return a0100;
	}
	public void setA0100(Long a0100) {
		this.a0100 = a0100;
	}
	public String getA0101() {
		return a0101;
	}
	public void setA0101(String a0101) {
		this.a0101 = a0101;
	}
	public String getA0102() {
		return a0102;
	}
	public void setA0102(String a0102) {
		this.a0102 = a0102;
	}
	public String getA01k29() {
		return a01k29;
	}
	public void setA01k29(String a01k29) {
		this.a01k29 = a01k29;
	}
	public String getA0104() {
		return a0104;
	}
	public void setA0104(String a0104) {
		this.a0104 = a0104;
	}
	public String getA0134() {
		return a0134;
	}
	public void setA0134(String a0134) {
		this.a0134 = a0134;
	}
	public String getA01k66() {
		return a01k66;
	}
	public void setA01k66(String a01k66) {
		this.a01k66 = a01k66;
	}
	public String getA0144() {
		return a0144;
	}
	public void setA0144(String a0144) {
		this.a0144 = a0144;
	}
	public String getA0141b() {
		return a0141b;
	}
	public void setA0141b(String a0141b) {
		this.a0141b = a0141b;
	}
	public String getA0141a() {
		return a0141a;
	}
	public void setA0141a(String a0141a) {
		this.a0141a = a0141a;
	}
	public String getA0837() {
		return a0837;
	}
	public void setA0837(String a0837) {
		this.a0837 = a0837;
	}
	public String getA0801n() {
		return a0801n;
	}
	public void setA0801n(String a0801n) {
		this.a0801n = a0801n;
	}
	public String getA0145() {
		return a0145;
	}
	public void setA0145(String a0145) {
		this.a0145 = a0145;
	}
	public String getA0104n() {
		return a0104n;
	}
	public void setA0104n(String a0104n) {
		this.a0104n = a0104n;
	}
	public String getA0117n() {
		return a0117n;
	}
	public void setA0117n(String a0117n) {
		this.a0117n = a0117n;
	}
	public String getA0107() {
		return a0107;
	}
	public void setA0107(String a0107) {
		this.a0107 = a0107;
	}
	public String getA0111n() {
		return a0111n;
	}
	public void setA0111n(String a0111n) {
		this.a0111n = a0111n;
	}
	public String getA01k33() {
		return a01k33;
	}
	public void setA01k33(String a01k33) {
		this.a01k33 = a01k33;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

	

}