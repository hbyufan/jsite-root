package com.jsite.modules.sys.web;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jsite.common.idgen.IdGenerate;
import com.jsite.modules.sys.entity.XZ59app;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jsite.common.config.Global;
import com.jsite.common.lang.StringUtils;
import com.jsite.common.persistence.Page;
import com.jsite.common.web.BaseController;
import com.jsite.modules.sys.entity.Organ;
import com.jsite.modules.sys.entity.XZ59;
import com.jsite.modules.sys.service.XZ59Service;


@Controller
@RequestMapping(value = "${adminPath}/sys/xz59list")
public class XZ59Controller  extends BaseController {
	@Autowired
	private XZ59Service xz59Service;
	
	@ModelAttribute
	public XZ59 get(@RequestParam(required=false) String id) {
		XZ59 entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = xz59Service.get(id);
		}
		if (entity == null){
			entity = new XZ59();
		}
		return entity;
	}
	
	@RequiresPermissions("sys:user:view")
	@RequestMapping(value = {"index", ""})
	public String list() {
		return "modules/sys/xz59Index";
	}
	
	
	//页面list
	@RequiresPermissions("sys:user:view")
	@RequestMapping(value = {"listData"},method= RequestMethod.POST)
	 @ResponseBody
	public Page<XZ59> listData(XZ59 xz59, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<XZ59> page = xz59Service.findxz59(new Page<XZ59>(request, response), xz59);
		System.out.println(page);
		return page;
	}
	
	
	/**
	 * 
	 * 查询全部首页
	 * @param request
	 * @return
	 */
    @RequestMapping(value = "findAllList", method= RequestMethod.GET)
    @ResponseBody
    public String findAllList(HttpServletRequest request) {
		String tableName=request.getParameter("table");
		String code=request.getParameter("code");
		try {
			HashMap<String,Object> param = new HashMap<String,Object>();
			if(tableName!=null && !tableName.equals("")){
				param.put("tableName",tableName);
				if(code!=null && !code.equals("")){
					param.put("code",code);
				}
			}else {
				return "";
			}
			List<XZ59app> pageResult = xz59Service.findAllList(param);
			return responseSuccess(pageResult);
    	} catch (Exception e) {
			return e.getMessage();
		}
    }  
    
    /**
     * 
     * 添加首页
     * 
     * 
     */
    @RequiresPermissions("sys:user:edit")
	@RequestMapping(value = {"from"})
	public String form() {
		return "modules/sys/xz59Form";
	}
    
    /**
     * 保存信息
     */
	@RequiresPermissions("sys:user:edit")
	@RequestMapping(value = {"addXz"})
	@ResponseBody
	public String addXz(XZ59 xZ59){
		xZ59.setId(IdGenerate.uuid());
		xz59Service.addXz(xZ59);
		return renderResult(Global.TRUE,"添加首页信息'" + xZ59.getText()+ "'成功");
	}
	
	/**
	 * 修改首页信息
	 */
	@RequiresPermissions("sys:user:edit")
	@RequestMapping(value = {"editXz"})
	@ResponseBody
	public String editXz(XZ59 xZ59){
		xz59Service.editXz(xZ59);
		return renderResult(Global.TRUE,"修改首页信息'" + xZ59.getText()+ "'成功");
	}
	
	/**
	 * 跳转到修改页面
	 */
	@RequiresPermissions("sys:user:view")
	@RequestMapping(value = "formXZ")
	public String formXZ(Model model,XZ59 xZ59) {
		model.addAttribute("xZ59",xZ59);
		return "modules/sys/xz59Edit";
	}
	
	/**
	 * 删除首页信息
	 */
	
	@RequiresPermissions("sys:user:edit")
	@RequestMapping(value = "deleteXZ")
	@ResponseBody
    public String deleteXZ(XZ59 xz59) {
		xz59Service.deleteXZ(xz59);
        return renderResult(Global.TRUE, "删除首页信息成功");
    }
  
}



