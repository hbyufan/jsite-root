package com.jsite.modules.sys.entity;

import com.jsite.common.persistence.DataEntity;

/**
 * 机构的实体类
 * @author yj
 *
 */
public class Organ {
	

	private Integer id;
	private String OrganName; //机构名称
	private String OrganJson; //机构存放的session
	private String code; //机构存放的session
	
	

	public Organ() {
		super();
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getOrganName() {
		return OrganName;
	}
	public void setOrganName(String organName) {
		OrganName = organName;
	}
	public String getOrganJson() {
		return OrganJson;
	}
	public void setOrganJson(String organJson) {
		OrganJson = organJson;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "Organ [id=" + id + ", OrganName=" + OrganName + ", OrganJson=" + OrganJson + "]";
	}
}
