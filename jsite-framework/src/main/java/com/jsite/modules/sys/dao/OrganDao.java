package com.jsite.modules.sys.dao;

import java.util.List;

import com.jsite.common.persistence.annotation.MyBatisDao;
import com.jsite.modules.sys.entity.Organ;

/**
 * 机构调整的实体类
 * @author yj
 *
 */
@MyBatisDao
public interface OrganDao {

	//单条查询
	public Organ getCode(String code);
	//机构信息显示
	public List<Organ> findOrgan();
	//机构信息修改
	public void updateOrgan(Organ organ);
	//删除机构信息
	public void deleteOrgan(Organ organ);
	//增加机构信息
	public void addOrgan(Organ organ);
}
