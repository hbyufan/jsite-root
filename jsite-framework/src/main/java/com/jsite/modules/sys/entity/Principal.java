package com.jsite.modules.sys.entity;

import com.jsite.modules.sys.utils.UserUtils;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;

/**
 * 授权用户信息
 * @author liuruijun
 * @version 2018.10.22
 */
public class Principal implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id; // 编号
    private String loginName; // 登录名
    private String name; // 姓名
    private String userType;// 用户类型
    private String imei;//判断是否imei
    private boolean mobileLogin; // 是否手机登录
    private  Office office;//机构

//		private Map<String, Object> cacheMap;

    public Principal(User user, boolean mobileLogin) {
        this.id = user.getId();
        this.loginName = user.getLoginName();
        this.name = user.getName();
        this.userType = user.getUserType();
        this.mobileLogin = mobileLogin;
        this.imei = user.getImei();
        this.office = user.getOffice();
    }

    public String getId() {
        return id;
    }

    public String getLoginName() {
        return loginName;
    }

    public String getName() {
        return name;
    }
    
    
    public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getUserType() {return userType; }

    public boolean isMobileLogin() {
        return mobileLogin;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    //		@JsonIgnore
//		public Map<String, Object> getCacheMap() {
//			if (cacheMap==null){
//				cacheMap = new HashMap<String, Object>();
//			}
//			return cacheMap;
//		}

    /**
     * 获取SESSIONID
     */
    public String getSessionid() {
        try{
            return (String) UserUtils.getSession().getId();
        }catch (Exception e) {
            return "";
        }
    }

    @Override
    public String toString() {
        return "Principal{" +
                "id='" + id + '\'' +
                ", loginName='" + loginName + '\'' +
                ", name='" + name + '\'' +
                ", userType='" + userType + '\'' +
                ", imei='" + imei + '\'' +
                ", mobileLogin=" + mobileLogin +
                ", office='" + office + '\'' +
                '}';
    }
}

