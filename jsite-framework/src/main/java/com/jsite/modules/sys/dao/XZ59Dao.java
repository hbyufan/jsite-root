/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jsite.modules.sys.dao;

import java.util.HashMap;
import java.util.List;

import com.jsite.common.persistence.CrudDao;
import com.jsite.common.persistence.Page;
import com.jsite.common.persistence.annotation.MyBatisDao;
import com.jsite.modules.sys.entity.User;
import com.jsite.modules.sys.entity.XZ59;
import com.jsite.modules.sys.entity.XZ59app;

/**
 * 区域DAO接口
 * @author ThinkGem
 * @version 2014-05-16
 */
@MyBatisDao
public interface XZ59Dao extends CrudDao<XZ59> {
	/**/
	//查询全部用于app和后台显示
	public List<XZ59app> findAllList(HashMap<String,Object> param);
	
	public List<XZ59> findxz59(XZ59 xz59);

	/**
	 * 删除用户角色关联数据
	 * @param user
	 * @return
	 */
	public int deleteUserRole(User user);
	
	/**
	 * 插入用户角色关联数据
	 * @param user
	 * @return
	 */
	public int insertUserRole(User user);
	
	/**
	 * 更新用户信息
	 * @param user
	 * @return
	 */
	public int updateUserInfo(User user);
	
	/**
	 * 添加用户信息
	 * @param xZ59
	 */
	public void addXz(XZ59 xZ59);
	
	/**
	 * 修改首页信息
	 * @param xZ59
	 */
	public void editXz(XZ59 xZ59);

	/**
	 *删除首页信息
	 * @param xz59
	 */
	public void deleteXZ(XZ59 xz59);
}

