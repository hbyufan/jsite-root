package com.jsite.modules.sys.dao;

/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
import com.jsite.common.persistence.CrudDao;
import com.jsite.common.persistence.annotation.MyBatisDao;
import com.jsite.modules.sys.entity.UserZZ;

import java.util.List;

/**
 * 用户DAO接口
 * @author ThinkGem
 * @version 2014-05-16
 */
@MyBatisDao
public interface UserZZDao extends CrudDao<UserZZ> {
	
	/**
	 * 查询全部用户进行分析
	 * @param loginName
	 * @return
	 */
	public UserZZ findAllbyUser(UserZZ userZZ);


	

}

