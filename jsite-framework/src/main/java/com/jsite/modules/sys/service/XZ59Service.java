/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jsite.modules.sys.service;
import com.jsite.common.persistence.BaseEntity;
import com.jsite.common.persistence.Page;
import com.jsite.common.service.CrudService;
import com.jsite.modules.sys.dao.XZ59Dao;
import com.jsite.modules.sys.entity.User;
import com.jsite.modules.sys.entity.XZ59;

import com.jsite.modules.sys.entity.XZ59app;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/** 
 * 区域Service
 * @author ThinkGem
 * @version 2014-05-16
 */
@Service
@Transactional(readOnly = true)
public class XZ59Service extends CrudService<XZ59Dao, XZ59>  {
	@Autowired
	private XZ59Dao xz59dao;
	public List<XZ59app> findAllList(HashMap<String,Object> param) {
		return  xz59dao.findAllList(param);
	
	
}
	public XZ59 get(String id) {
		return super.get(id);
	}
	public Page<XZ59> findxz59(Page<XZ59> page, XZ59 xz59) {
		// TODO 自动生成的方法存根
		   xz59.setPage(page);
	        // 执行分页查询
	        List<XZ59> userList = xz59dao.findxz59(xz59);
	        page.setList(userList);
	        return page;
	}
	/**
	 * 添加信息
	 * @param xZ59
	 */
	@Transactional(readOnly = false)
	public void addXz(XZ59 xZ59) {
		xz59dao.addXz(xZ59);
	}
	
	
	/**
	 * 修改首页信息
	 * @param xZ59
	 */
	@Transactional(readOnly = false)
	public void editXz(XZ59 xZ59) {
		xz59dao.editXz(xZ59);
	}
	
	/**
	 * 删除首页信息
	 * @param xz59
	 */
	@Transactional(readOnly = false)
	public void deleteXZ(XZ59 xz59) {
		xz59dao.deleteXZ(xz59);
	}
}