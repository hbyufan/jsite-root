package com.jsite.modules.sys.entity;

import com.jsite.common.persistence.DataEntity;

public class XZ59 extends DataEntity<XZ59>{
	protected String id;
	protected String ide;
	protected String pid; // 所有父级编号
	protected String text; 	// 机构名称
	protected String remark;// sql
	protected String remark1;// sql
	protected String remark2;// sql
	protected String code;// 权限区域
	public String getId() {
		return id;
	}
	public String getIde() {
		return ide;
	}
	public void setIde(String ide) {
		this.ide = ide;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRemark1() {
		return remark1;
	}
	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}
	public String getRemark2() {
		return remark2;
	}
	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
}
