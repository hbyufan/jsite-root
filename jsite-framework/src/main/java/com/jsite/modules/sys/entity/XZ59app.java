package com.jsite.modules.sys.entity;

import com.jsite.common.persistence.DataEntity;

public class XZ59app extends DataEntity<XZ59app>{
	protected String id;
	protected String pid; // 所有父级编号
	protected String text; 	// 机构名称
	protected String remark;// sql
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}
