package com.jsite.modules.sys.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.jsite.common.utils.BeanUtil;
import com.jsite.common.utils.PagedResult;
import com.jsite.modules.sys.dao.UserZZListDao;
import com.jsite.modules.sys.entity.UserZZList;
import java.util.HashMap;

@Service
@Transactional(readOnly = true)
public class UserZZListService {
	
	@Autowired
	private UserZZListDao userZZListDao;
	/***
	 * 通用查询传sql
	 * @param param
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	 public PagedResult<UserZZList> findUserbySenior(HashMap<String, Object> param,Integer pageNo,Integer pageSize){
		 pageNo = pageNo == null?1:pageNo+1;
			pageSize = pageSize == null?10:pageSize;
			PageHelper.startPage(pageNo,pageSize);  //startPage是告诉拦截器说我要开始分页了。分页参数是这两个。
			return BeanUtil.toPagedResult(userZZListDao.findUserbySenior(param));
	 }

	 
	 /***
		 * 机构条件查询
		 * @param param
		 * @param pageNo
		 * @param pageSize
		 * @return
		 */
	 public PagedResult<UserZZList> findSpellByKey(HashMap<String, Object> param,Integer pageNo,Integer pageSize){
		 pageNo = pageNo == null?1:pageNo+1;
			pageSize = pageSize == null?10:pageSize;
			PageHelper.startPage(pageNo,pageSize);  //startPage是告诉拦截器说我要开始分页了。分页参数是这两个。
			return BeanUtil.toPagedResult(userZZListDao.findSpellByKey(param));
	 }
	
	
	
}