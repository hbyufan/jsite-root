/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jsite.modules.sys.dao;
import com.jsite.common.persistence.CrudDao;
import com.jsite.common.persistence.annotation.MyBatisDao;
import com.jsite.modules.sys.entity.UserZZList;

import java.util.HashMap;
import java.util.List;

/**
 * 区域DAO接口
 * @author ThinkGem
 * @version 2014-05-16
 */
@MyBatisDao
public interface UserZZListDao extends CrudDao<UserZZList> {
	/**/
	
	//查询全部用于app和后台显示
	public List<UserZZList> findUserbySenior(HashMap<String, Object> param);

	
	//查询机构显示
	public List<UserZZList> findSpellByKey(HashMap<String, Object> param);
	

}
