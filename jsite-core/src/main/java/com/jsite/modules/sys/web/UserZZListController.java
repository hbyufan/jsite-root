package com.jsite.modules.sys.web;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;

import com.jsite.common.security.shiro.session.SessionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jsite.common.utils.PagedResult;
import com.jsite.common.web.BaseController;
import com.jsite.modules.sys.entity.UserZZList;
import com.jsite.modules.sys.service.UserZZListService;


@Controller
@RequestMapping(value = "${adminPath}/sys/userzz")
public class UserZZListController extends BaseController {
	@Autowired
	private UserZZListService userzzService;



	/***
	 * 通用查询传sql
	 * @param param
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
    @RequestMapping(value = "findUserbySenior", method= RequestMethod.GET)
    @ResponseBody
    public String  findUserbySenior( HttpServletRequest request ) {
    	String  sql=request.getParameter("sql");
		System.out.println(sql);
    	Integer pageIndex=Integer.valueOf(request.getParameter("pageIndex"));
    	Integer pageSize=Integer.valueOf(request.getParameter("pageSize"));
    	logger.info("分页查询用户信息列表请求入参：pageIndex{},pageSize{}", pageIndex,pageSize);
    	try {
			HashMap<String,Object> param = new HashMap<String,Object>();
			param.put("sql",sql);
			PagedResult<UserZZList> pageResult = userzzService.findUserbySenior(param,pageIndex,pageSize);
    	    return responseSuccess(pageResult);
    	} catch (Exception e) {
    		e.printStackTrace();
			return e.getMessage();
		}
    }
    
    /***
	 * 机构条件查询
	 * @param param
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */

    @RequestMapping(value = "findSpellByKey", method= RequestMethod.GET)
    @ResponseBody
    public String  findSpellByKey(HttpServletRequest request ) {
    	String  deptId=request.getParameter("deptId");
    	String  ctn=request.getParameter("ctn");
    	Integer pageIndex=Integer.valueOf(request.getParameter("pageIndex"));
    	Integer pageSize=Integer.valueOf(request.getParameter("pageSize"));
    	logger.info("分页查询用户信息列表请求入参：pageIndex{},pageSize{}", pageIndex,pageSize);
    	try {
			HashMap<String,Object> param = new HashMap<String,Object>();
			//param.put("sql",sql);
			param.put("ctn", ctn);
			param.put("deptId", deptId);
			PagedResult<UserZZList> pageResult = userzzService.findSpellByKey(param,pageIndex,pageSize);
    	    return responseSuccess(pageResult);
    	} catch (Exception e) {
    		e.printStackTrace();
			return e.getMessage();
		}
    }
    
    
}