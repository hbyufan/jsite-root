package com.jsite.modules.sys.web;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jsite.common.security.shiro.session.SessionDAO;
import com.jsite.common.utils.PagedResult;
import com.jsite.modules.sys.entity.UserZZList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jsite.common.web.BaseController;
import com.jsite.modules.sys.entity.UserZZ;
import com.jsite.modules.sys.service.UserZZService;


@Controller
@RequestMapping(value = "${adminPath}/sys/userzz")
public class UserZZController extends BaseController {
	@Autowired
	private UserZZService userzzService;

    @RequestMapping(value = "findAnalysis", method= RequestMethod.GET)
    @ResponseBody
    public String  findAnalysis(String code) {
		System.out.println(code);
		String codew=code+"%";
    	try {
			//HashMap<String,Object> param = new HashMap<String,Object>();
			//param.put("sql","");
			List<UserZZ> pageResult = userzzService.findAnalysis(codew);
    	    return responseSuccess(dataProcessing(pageResult));
    	} catch (Exception e) {
    		e.printStackTrace();
			return e.getMessage();
		}
    }
    
    @RequestMapping(value = "findPerson", method= RequestMethod.GET)
    @ResponseBody
    public String  findPerson(HttpServletRequest request) {
    	String sql =  request.getParameter("sql");
    	//System.out.println("sql"+sql);
    	try {
			HashMap<String,Object> param = new HashMap<String,Object>();
			param.put("sql",sql);
			List<UserZZ> pageResult = userzzService.findPerson(param);
    	    return responseSuccess(pageResult);
    	} catch (Exception e) {
    		e.printStackTrace();
			return e.getMessage();
		}
    }
    //frame-
    @RequestMapping(value = "findAnalysisal", method= RequestMethod.GET)
    @ResponseBody
    public String  findAnalysisal(HttpServletRequest request) {
    	 String sql= request.getParameter("sql"); //sql语句
    	try {
			HashMap<String,Object> param = new HashMap<String,Object>();
			param.put("sql",sql);
			List<UserZZ> pageResult = userzzService.findAnalysisal(param);
    	    return responseSuccess(dataProcessing(pageResult));
    	} catch (Exception e) {
    		e.printStackTrace();
			return e.getMessage();
		}
    }

	/***
	 * 机构条件查询数据分析
	 * @return
	 */

	@RequestMapping(value = "findAnalysisalms", method= RequestMethod.GET)
	@ResponseBody
	public String  findAnalysisalms(HttpServletRequest request ) {
		String  deptId=request.getParameter("deptId");
		String  ctn=request.getParameter("ctn");
		try {
			HashMap<String,Object> param = new HashMap<String,Object>();
			//param.put("sql",sql);
			param.put("ctn", ctn);
			param.put("deptId", deptId);
			List<UserZZ> pageResult = userzzService.findAnalysisalms(param);
			return responseSuccess(dataProcessing(pageResult));
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
    //数据处理
    public List  dataProcessing(List<UserZZ> pageResult ){
    	List list=new ArrayList();
		Map<String,Map<String,Integer>> m1=new HashMap();
		Map<String,Map<String,Integer>> m2=new HashMap();
		Map<String,Map<String,Integer>> m3=new HashMap();
		Map<String,Map<String,Integer>> m4=new HashMap();
		Map<String,Integer> m5=new HashMap();

		Map<String,Integer> sm=null;
		String[] str1  = {"A","B","C","D","E","F","G"};
		String[] str2 =  {"A","B","C","D","E","F","G"};
		String[] str3 =  {"A","B","C","D","E","F","G"};
		String[] str4 =  {"A","B","C","D","E","F","G"};
		
		for(String s:str1){
			sm=new HashMap(); 
			sm.put("A", 0);
			sm.put("B", 0);
			sm.put("C", 0);
			sm.put("D", 0);
			sm.put("E", 0);
			sm.put("F", 0);
			sm.put("G", 0);
			m1.put(s, sm);
		}
		
		for(String s:str2){
			sm=new HashMap(); 
			sm.put("A", 0);
			sm.put("B", 0);
			sm.put("C", 0);
			sm.put("D", 0);
			sm.put("E", 0);
			sm.put("F", 0);
			sm.put("G", 0);
			m2.put(s, sm);
		}
		
		for(String s:str3){
			sm=new HashMap(); 
			sm.put("A", 0);
			sm.put("B", 0);
			sm.put("C", 0);
			sm.put("D", 0);
			sm.put("E", 0);
			sm.put("F", 0);
			sm.put("G", 0);
			m3.put(s, sm);
		}
		
		for(String s:str4){
			sm=new HashMap(); 
			sm.put("A", 0);
			sm.put("B", 0);
			sm.put("C", 0);
			sm.put("D", 0);
			sm.put("E", 0);
			sm.put("F", 0);
			sm.put("G", 0);
			m4.put(s, sm);
		}
		
		int sexX=0;
		int sexY=0;
		int mz=0;
		int dp=0;
		for(UserZZ z:pageResult){
			if(!"汉族".equals(z.getA0117n())){
				mz++;
			}
			if(!"01".equals(z.getA0141a())){
				dp++;
			}
			
			String f="B";
			if("男".equals(z.getA0104n())){
				sexX++;
				f="A";
			}else{
				sexY++;
			}
            //System.out.println("222222222222");
			if("0131".equals(z.getA0193()) || "0831".equals(z.getA0193())){
				m3.get(f).put("A", m3.get(f).get("A")+1);
			}else if("0132".equals(z.getA0193()) ||"0832".equals(z.getA0193())){
				m3.get(f).put("B", m3.get(f).get("B")+1);
			}else if("0121".equals(z.getA0193()) ||"0821".equals(z.getA0193())){
				m3.get(f).put("C",  m3.get(f).get("C")+1);
			}else if("0122".equals(z.getA0193()) ||"0822".equals(z.getA0193())){
				m3.get(f).put("D",  m3.get(f).get("D")+1);
			}else if("0141".equals(z.getA0193()) ||  "0841".equals(z.getA0193())){
				m3.get(f).put("E",  m3.get(f).get("E")+1);
			}else if("0142".equals(z.getA0193()) ||  "0842".equals(z.getA0193())){
				m3.get(f).put("F",  m3.get(f).get("F")+1);
			}else{
				m3.get(f).put("G",  m3.get(f).get("G")+1);
				
			}
			
			String t="";
			if(z.getA0801()!=null && !z.getA0801().equals("")){
				if ("11".equals(z.getA0801()) || "1".equals(z.getA0801())
						|| "13".equals(z.getA0801()) || "1A".equals(z.getA0801())
						|| "1B".equals(z.getA0801()) || "12".equals(z.getA0801())) {
					t = "E";
				} else if ("2".equals(z.getA0801()) || "21".equals(z.getA0801()) || "2A".equals(z.getA0801()) || "2B".equals(z.getA0801())) {
					t = "D";
				} else if ("31".equals(z.getA0801()) || "34".equals(z.getA0801()) || "37".equals(z.getA0801()) || "3B".equals(z.getA0801())) {
					t = "C";
				} else if ("41".equals(z.getA0801()) || "47".equals(z.getA0801())) {
					t = "B";
				} else if ("81".equals(z.getA0801()) || "71".equals(z.getA0801()) || "61".equals(z.getA0801())) {
					t = "A";
				}
				if (!"".equals(t)) {
					if("0131".equals(z.getA0193()) || "0831".equals(z.getA0193())){
						m4.get(t).put("A", m4.get(t).get("A")+1);
					}else if("0132".equals(z.getA0193()) ||"0832".equals(z.getA0193())){
	    				m4.get(t).put("B", m4.get(t).get("B")+1);
					}else if("0121".equals(z.getA0193()) ||"0821".equals(z.getA0193())){
	    				m4.get(t).put("C",  m4.get(t).get("C")+1);
					}else if("0122".equals(z.getA0193()) ||"0822".equals(z.getA0193())){
	    				m4.get(t).put("D",  m4.get(t).get("D")+1);
					}else if("0141".equals(z.getA0193()) ||  "0841".equals(z.getA0193())){
	    				m4.get(t).put("E",  m4.get(t).get("E")+1);
					}else if("0142".equals(z.getA0193()) ||  "0842".equals(z.getA0193())){
	    				m4.get(t).put("F",  m4.get(t).get("F")+1);
	    			}else{
	    				m4.get(t).put("G",  m4.get(t).get("G")+1);
	    				
	    			}
				}
			}
			
			String d="C";
			if(z.getA0141a()==null){
				
			}else if("01".equals(z.getA0141a()) || "02".equals(z.getA0141a())){
				d="A";
			}else if("03".equals(z.getA0141a())){
				d="B";
			}else if("12".equals(z.getA0141a())){
				d="D";
			}else if("13".equals(z.getA0141a())){
				d="E";
			}
			if("0131".equals(z.getA0193()) || "0831".equals(z.getA0193())){
				m2.get(d).put("A", m2.get(d).get("A")+1);
			}else if("0132".equals(z.getA0193()) || "0832".equals(z.getA0193())){
				m2.get(d).put("B", m2.get(d).get("B")+1);
			}else if("0121".equals(z.getA0193()) ||  "0821".equals(z.getA0193())){
				m2.get(d).put("C",  m2.get(d).get("C")+1);
			}else if("0122".equals(z.getA0193()) ||  "0822".equals(z.getA0193())){
				m2.get(d).put("D",  m2.get(d).get("D")+1);
			}else if("0141".equals(z.getA0193()) ||  "0841".equals(z.getA0193())){
				m2.get(d).put("E",  m2.get(d).get("E")+1);
			}else if("0142".equals(z.getA0193()) ||  "0842".equals(z.getA0193())){
				m2.get(d).put("F",  m2.get(d).get("F")+1);
			}else{
				m2.get(d).put("G",  m2.get(d).get("G")+1);
				
			}
			
			if(z.getA6137()!=null && !z.getA6137().equals("")){
				Long age=z.getA6137();
				String k="";
				if(age>55){
					k="G";
				}else if(age>50 && age<56){
					k="F";
				}else if(age>45 && age<51){
					k="E";
				}else if(age>40 && age<46){
					k="D";
				}else if(age>35 && age<41){
					k="C";
				}else if(age>30 && age<36){
					k="B";
				}else if(age<=30){
					k="A";
				}
                if("0131".equals(z.getA0193()) || "0831".equals(z.getA0193())){
					m1.get(k).put("A", m1.get(k).get("A")+1);
                }else if("0132".equals(z.getA0193()) || "0832".equals(z.getA0193())){
    				m1.get(k).put("B", m1.get(k).get("B")+1);
                }else if("0121".equals(z.getA0193()) ||  "0821".equals(z.getA0193())){
    				m1.get(k).put("C",  m1.get(k).get("C")+1);
                }else if("0122".equals(z.getA0193()) ||  "0822".equals(z.getA0193())){
    				m1.get(k).put("D",  m1.get(k).get("D")+1);
                }else if("0141".equals(z.getA0193()) ||  "0841".equals(z.getA0193())){
    				m1.get(k).put("E",  m1.get(k).get("E")+1);
                }else if("0142".equals(z.getA0193()) ||  "0842".equals(z.getA0193())){
    				m1.get(k).put("F",  m1.get(k).get("F")+1);
    			}else{
    				m1.get(k).put("G",  m1.get(k).get("G")+1);
    				
    			}
			}
			
		}
		m5.put("total", pageResult.size());//总数
		m5.put("sexY", sexY);//女性
		m5.put("mz", mz);//少数民族
		m5.put("dp", dp);//非共产党员

		list.add(m1);
		list.add(m2);
		list.add(m3);
		list.add(m4);
		list.add(m5);
    	return list;
    }
  
}