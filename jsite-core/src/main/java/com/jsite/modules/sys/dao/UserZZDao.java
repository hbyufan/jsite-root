/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jsite.modules.sys.dao;
import com.jsite.common.persistence.CrudDao;
import com.jsite.common.persistence.annotation.MyBatisDao;
import com.jsite.modules.sys.entity.UserZZ;

import java.util.HashMap;
import java.util.List;

/**
 * 区域DAO接口
 * @author ThinkGem
 * @version 2014-05-16
 */
@MyBatisDao
public interface UserZZDao extends CrudDao<UserZZ> {
	/**/
	
	//查询全部用于app和后台显示
	public List<UserZZ> findAnalysis(String code);
	//带条件分析
	public List<UserZZ> findAnalysisal(HashMap<String, Object> param);
	//机构分析
	public List<UserZZ> findAnalysisalms(HashMap<String, Object> param);
	//查询个人
	public List<UserZZ> findPerson(HashMap<String, Object> param);
}
