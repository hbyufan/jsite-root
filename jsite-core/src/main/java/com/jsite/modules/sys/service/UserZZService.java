package com.jsite.modules.sys.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.jsite.modules.sys.dao.UserZZDao;
import com.jsite.modules.sys.entity.UserZZ;

import java.util.HashMap;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class UserZZService {
	
	@Autowired
	private UserZZDao userZZDao;
	//查询全部用于app和后台显示
	 public List<UserZZ> findAnalysis(String code){
		// HashMap<String,Object> param = null;
		// System.out.println("1111");
		 List<UserZZ>  analysis= userZZDao.findAnalysis(code);
		return analysis; 
	 }
	//带条件分析
	public List<UserZZ> findAnalysisal(HashMap<String, Object> param) {
		// TODO 自动生成的方法存根
		 List<UserZZ>  analysis= userZZDao.findAnalysisal(param);
			return analysis; 
	}
	//机构分析
	public List<UserZZ> findAnalysisalms(HashMap<String, Object> param) {
		// TODO 自动生成的方法存根
		List<UserZZ>  analysis= userZZDao.findAnalysisalms(param);
		return analysis;
	}
	//查询个人
	public List<UserZZ> findPerson(HashMap<String, Object> param) {
		// TODO 自动生成的方法存根
		 List<UserZZ>  analysis= userZZDao.findPerson(param);
			return analysis; 
	}
	
	
}