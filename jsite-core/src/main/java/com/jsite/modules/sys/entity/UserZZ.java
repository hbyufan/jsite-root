package com.jsite.modules.sys.entity;


/**
 * 
 * @author huangxz
 *
 */
public class UserZZ  {
	private static final long serialVersionUID = 1L;
	//alias
	public static final String TABLE_ALIAS = "UserZZ";
	public static final String ALIAS_A0100 = "a0100";
	public static final String ALIAS_A0101 = "a0101";
	public static final String ALIAS_A0102 = "a0102";
	public static final String ALIAS_A01K29 = "a01k29";
	public static final String ALIAS_A0104 = "a0104";
	public static final String ALIAS_A0134 = "a0134";
	public static final String ALIAS_A01K66 = "a01k66";
	public static final String ALIAS_A0144 = "a0144";
	public static final String ALIAS_A0141B = "a0141b";
	public static final String ALIAS_A0141A = "a0141a";
	public static final String ALIAS_A0837 = "a0837";
	public static final String ALIAS_A0801N = "a0801n";
	public static final String ALIAS_A0145 = "a0145";
	public static final String ALIAS_A0104N = "a0104n";
	public static final String ALIAS_A0117N = "a0117n";
	public static final String ALIAS_A0107 = "a0107";
	public static final String ALIAS_A0111N = "a0111n";
	public static final String ALIAS_A01K33 = "a01k33";
	public static final String ALIAS_A0801 = "a0801";
	public static final String ALIAS_A0193 = "a0193";
	public static final String ALIAS_A0141 = "a0141";
	public static final String ALIAS_A6137 = "a6137";
	//date formats
	
	//columns START
	private java.lang.Long a0100;
	private java.lang.String a0101;
	private java.lang.String a0102;
	private java.lang.String a01k29;
	private java.lang.String a0104;
	private java.lang.String a0134;
	private java.lang.String a01k66;
	private java.lang.String a0144;
	private java.lang.String a0141b;
	private java.lang.String a0141a;
	private java.lang.String a0837;
	private java.lang.String a0801n;
	private java.lang.String a0145;
	private java.lang.String a0104n;
	private java.lang.String a0117n;
	private java.lang.String a0107;
	private java.lang.String a0111n;
	private java.lang.String a01k33;
	private java.lang.String a0801;
	private java.lang.String a0193;
	private java.lang.String a0141;
	private java.lang.Long a6137;
	public void setA0100(java.lang.Long a0100) {
		this.a0100 = a0100;
	}
	public void setA0101(java.lang.String a0101) {
		this.a0101 = a0101;
	}
	public void setA0102(java.lang.String a0102) {
		this.a0102 = a0102;
	}
	public void setA01k29(java.lang.String a01k29) {
		this.a01k29 = a01k29;
	}
	public void setA0104(java.lang.String a0104) {
		this.a0104 = a0104;
	}
	public void setA0134(java.lang.String a0134) {
		this.a0134 = a0134;
	}
	public void setA01k66(java.lang.String a01k66) {
		this.a01k66 = a01k66;
	}
	public void setA0144(java.lang.String a0144) {
		this.a0144 = a0144;
	}
	public void setA0141b(java.lang.String a0141b) {
		this.a0141b = a0141b;
	}
	public void setA0141a(java.lang.String a0141a) {
		this.a0141a = a0141a;
	}
	public void setA0837(java.lang.String a0837) {
		this.a0837 = a0837;
	}
	public void setA0801n(java.lang.String a0801n) {
		this.a0801n = a0801n;
	}
	public void setA0145(java.lang.String a0145) {
		this.a0145 = a0145;
	}
	public void setA0104n(java.lang.String a0104n) {
		this.a0104n = a0104n;
	}
	public void setA0117n(java.lang.String a0117n) {
		this.a0117n = a0117n;
	}
	public void setA0107(java.lang.String a0107) {
		this.a0107 = a0107;
	}
	public void setA0111n(java.lang.String a0111n) {
		this.a0111n = a0111n;
	}
	public void setA01k33(java.lang.String a01k33) {
		this.a01k33 = a01k33;
	}
	public java.lang.Long getA0100() {
		return a0100;
	}
	public java.lang.String getA0101() {
		return a0101;
	}
	public java.lang.String getA0102() {
		return a0102;
	}
	public java.lang.String getA01k29() {
		return a01k29;
	}
	public java.lang.String getA0104() {
		return a0104;
	}
	public java.lang.String getA0134() {
		return a0134;
	}
	public java.lang.String getA01k66() {
		return a01k66;
	}
	public java.lang.String getA0144() {
		return a0144;
	}
	public java.lang.String getA0141b() {
		return a0141b;
	}
	public java.lang.String getA0141a() {
		return a0141a;
	}
	public java.lang.String getA0837() {
		return a0837;
	}
	public java.lang.String getA0801n() {
		return a0801n;
	}
	public java.lang.String getA0145() {
		return a0145;
	}
	public java.lang.String getA0104n() {
		return a0104n;
	}
	public java.lang.String getA0117n() {
		return a0117n;
	}
	public java.lang.String getA0107() {
		return a0107;
	}
	public java.lang.String getA0111n() {
		return a0111n;
	}
	public java.lang.String getA01k33() {
		return a01k33;
	}
	public java.lang.String getA0801() {
		return a0801;
	}
	public void setA0801(java.lang.String a0801) {
		this.a0801 = a0801;
	}
	public java.lang.String getA0193() {
		return a0193;
	}
	public void setA0193(java.lang.String a0193) {
		this.a0193 = a0193;
	}
	public java.lang.String getA0141() {
		return a0141;
	}
	public void setA0141(java.lang.String a0141) {
		this.a0141 = a0141;
	}
	public Long getA6137() {
		return a6137;
	}
	public void setA6137(Long a6137) {
		this.a6137 = a6137;
	}
}

