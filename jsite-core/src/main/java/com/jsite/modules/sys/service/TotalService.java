/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jsite.modules.sys.service;
import com.jsite.modules.sys.dao.TotalDao;
import com.jsite.modules.sys.dao.XZ59Dao;
import com.jsite.modules.sys.entity.Total;
import com.jsite.modules.sys.entity.XZ59;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/** 
 * 区域Service
 * @author ThinkGem
 * @version 2014-05-16
 */
@Service
@Transactional(readOnly = true)
public class TotalService   {
	@Autowired
	private TotalDao totalDao;
	public Total findAlltotal(HashMap<String, Object> param ) {
		
		return  totalDao.findAlltotal(param);
	
	}
}