/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jsite.modules.sys.security;

import java.util.Map;

/**
 * 用户和密码（包含验证码）令牌类
 * @author ThinkGem
 * @version 2013-5-19
 */
public class UsernamePasswordToken extends org.apache.shiro.authc.UsernamePasswordToken {

	private static final long serialVersionUID = 1L;

	//private String captcha;
	private boolean mobileLogin;
	private String imei;
	private Map params;

	public UsernamePasswordToken() {
		super();
	}

	public UsernamePasswordToken(String username, char[] password,
			boolean rememberMe, String host,  boolean mobileLogin, Map params,String imei) {
		super(username, password, rememberMe, host);
		//this.captcha = captcha;
	//	this.imei = imei;
		this.mobileLogin = mobileLogin;
		this.imei=imei;
		this.params = params;
	}

	/*public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}
*/
	public boolean isMobileLogin() {
		return mobileLogin;
	}

	public Map getParams() {
		return params;
	}

	public void setParams(Map params) {
		this.params = params;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}
	
}