package com.jsite.common.utils;

import java.util.List;

import com.github.pagehelper.Page;

/**
 * 功能概要：
 * 
 * @author linbingwen
 * @since  2015年10月22日 
 */


public class BeanUtil {

    public static <T> PagedResult<T> toPagedResult(List<T> datas) {
        PagedResult<T> result = new PagedResult<T>();
        if (datas instanceof Page) {
            Page<T> page = (Page<T>) datas;
            //result.setPageNo(page.getPageNum());
            //result.setPageSize(page.getPageSize());
            result.setData(page.getResult());
            result.setTotal(page.getTotal());
            //result.setPages(page.getPages());
        }
        else {
            //result.setPageNo(1);
            //result.setPageSize(datas.size());
            result.setData(datas);
            result.setTotal(datas.size());
        }

        return result;
    }

}
